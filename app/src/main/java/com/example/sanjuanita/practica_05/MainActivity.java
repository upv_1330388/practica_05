package com.example.sanjuanita.practica_05;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.ImageView;
import android.view.View;
import android.view.View.OnClickListener;

public class MainActivity extends Activity {

    public final static String EXTRA_MESSAGE = "com.upv.holaandroid.MESSAGE";
    public static String message;

    Button button;
    ImageView image;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //addListenerOnButton();

    }

   /* public void addListenerOnButton() {
        image = (ImageView) findViewById(R.id.imageView1);
        button = (Button) findViewById(R.id.btnChangeImage);
        button.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {
                image.setImageResource(R.drawable.uvas); //nombre del archivo imagen
            }
        });
    }*/

    /** Llama cuando el usuario hace click en el botón Send */
    public void uvas(View view) {
        message="uvas";
        Intent intent = new Intent(this, Main2Activity.class);
        intent.putExtra(EXTRA_MESSAGE, message);
        startActivity(intent);
    }
    public void manzanas(View view) {
        message="manzanas";
        Intent intent = new Intent(this, Main2Activity.class);
        intent.putExtra(EXTRA_MESSAGE, message);
        startActivity(intent);
    }
    public void pina(View view) {
        message="pina";
        Intent intent = new Intent(this, Main2Activity.class);
        intent.putExtra(EXTRA_MESSAGE, message);
        startActivity(intent);
    }
    public void sandia(View view) {
        message="sandia";
        Intent intent = new Intent(this, Main2Activity.class);
        intent.putExtra(EXTRA_MESSAGE, message);
        startActivity(intent);
    }

}
