package com.example.sanjuanita.practica_05;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.content.Intent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;



public class Main2Activity extends AppCompatActivity {
    ImageView image;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        Intent intent = getIntent();
        String message = intent.getStringExtra(MainActivity.EXTRA_MESSAGE);

        image = (ImageView) findViewById(R.id.imageView1);

        TextView textView = new TextView(this);
        textView.setTextSize(20);


        if(message.contains("uvas" )){
            image.setImageResource(R.drawable.uvas);
        }
        else if(message.contains("pina" )){
             image.setImageResource(R.drawable.pina);

        }else if(message.contains("sandia" )){

            image.setImageResource(R.drawable.sandia);

        }else if(message.contains("manzanas" )){

            image.setImageResource(R.drawable.manzanas);

        }

    }
}
